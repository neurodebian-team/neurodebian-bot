"$PODMAN" run --rm -it --user podman --device /dev/fuse --device /dev/net/tun -v /srv/janitor/neurodebian-janitor/tools/bin/runsc:/usr/bin/runsc --security-opt unmask=/sys/fs/cgroup --cap-add CHOWN,DAC_OVERRIDE,FOWNER quay.io/podman/stable
sed -i '/disabled/d' /etc/containers/containers.conf
podman --runtime runsc --runtime-flag ignore-cgroups run --rm --network pasta docker.io/library/debian:sid-slim dmesg
chmod 0777 /sys/fs/cgroup  && chmod 0666 /sys/fs/cgroup/cgroup.subtree_control && podman --runtime runsc run --rm docker.io/library/debian:sid-slim mount
podman exec --user root --privileged -li ip link delete tap0

NOTE:
* unmasking cgroupfs is needed to keep its mount readwrite and prevent "Cannot add conmon to namespace" error
* 'ignore-cgroups' on runsc is used on host user and inside container to skip configuring root-level cgroup.
  See [this gVisor issue](https://github.com/google/gvisor/issues/4371). Downside is that `cpus` and other limits using cgroups under the hood are lost.
* '/dev/net/tun' needed to make nested pasta/slirp4netns work. With runsc, speed is ~450 kb/sec, while 11 mb/sec with crun.
* Outer container should not be run as unprivileged user (no `--user podman`) so runsc mounts /proc, /dev, /dev/pts etc
  NOTE: crun works properly so it is a runsc bug. Emulating mountpoints will help installing systemd but will not solve i/o speed
* Even with `CAP_SYS_ADMIN` added, or run directly from host user, runsc containers dont retain changes on non-bound mounted parts. Bug?

"$PODMAN" run --rm -it --user podman --device /dev/fuse --device /dev/net/tun -v /srv/janitor/neurodebian-janitor/tools/bin/runsc:/usr/bin/runsc --security-opt unmask=/sys/fs/cgroup --cpus 17.0 quay.io/podman/stable
"$PODMAN" exec -u root --privileged -l chown -R podman:podman /sys/fs/cgroup
"$PODMAN" exec -u root -l sed -i '/disabled/d' /etc/containers/containers.conf
"$PODMAN" exec -l podman --runtime runsc --runtime-flag debug --runtime-flag debug-log=/tmp/runsc/ --runtime-flag cpu-num-from-quota run --cpus 4.0 --rm docker.io/library/debian:sid-slim nproc

The "proc-mount" inconsistency is caused by race condition introduced in my PoC. In fact there are two /proc mounts then: one is bind-mounted by podman via oci spec because podman is nested but pidns is host and second is ours. Can we unshare pidns?
Patched runsc to mount /proc after bound procfs mount

Init containers in pod: we can either create a pod with `--share cgroups,uts,net,ipc` then create containers with `--pod pod-name --requires init-container-name` or play kube and replace container with `--replace --name init-container --pod pod-name`. NOTE: pod will show as Degraded. Creating containner with `create --init-ctr=always` makes proper init container.
kube play is not compatible with `--security-opt` or `--share` bit manual pod construction is.

"$PODMAN" pod create --network slirp4netns --share cgroup,ipc,net,uts --share-parent=false testpod
"$PODMAN" create --pod testpod --init-ctr=always --security-opt unmask=/sys/fs/cgroup --cap-add CHOWN docker.io/library/debian:sid-slim chown -R 1000:1000 /sys/fs/cgroup
"$PODMAN" create --pod testpod --user podman --device /dev/fuse --device /dev/net/tun -v /srv/janitor/neurodebian-janitor/tools/bin/runsc:/usr/bin/runsc --security-opt unmask=/sys/fs/cgroup --cpus 17.0 -it quay.io/podman/stable

That's wrong: nested podman complains on 'cgroup.subtree_control: device or resource busy'.

The working way is to assign CHOWN and chown the same namespace from inside:

"$PODMAN" pod start testpod
"$PODMAN" create --pod testpod --rm -it --user podman --cap-add CHOWN --device /dev/fuse --device /dev/net/tun -v /srv/janitor/neurodebian-janitor/tools/bin/runsc:/usr/bin/runsc --security-opt unmask=/sys/fs/cgroup --cpus 17.0 quay.io/podman/stable

For control container, we need busybox with minimal applet set built-in (sh, ...) and capsh used to drop ambient capabilities like CHOWN:

chown -R podman:podman /sys/fs/cgroup
capsh --noamb --shell=/usr/local/bin/dagu -- scheduler

-- OR --

start container as root and use:

exec busybox-static su - podman -s /bin/bash

so that inherited and ambient capabilities are dropped without capsh(1).

Wireguard in pod:

"$PODMAN" run --rm -it --cap-add NET_ADMIN --sysctl net.ipv4.conf.all.src_valid_mark=1 docker.io/procustodibus/wireguard /bin/bash
---
wgcf register
wgcf generwte
wg-quick up wgcf-profile.conf

Control container needs sh to trap signals to iterate over dagu workflows and stop them.

---
init-volume - Init container organizing persistent storage volume

The volume is mapped into `control` container as /persist` inside container with following hierarchy:
        # /persist
        # |- dagu
        # |- queues
        #  |- nd-new-release
        #   |- scheduled
        #   |- success
        #   |- failure
        #  |- nd-import-sid
        #   |- scheduled
        #   |- success
        #   |- failure
        #  |- ...

---

"$PODMAN" run --user podman --device /dev/fuse --device /dev/net/tun -v /srv/janitor/neurodebian-janitor/tools/bin/runsc:/usr/bin/runsc --security-opt unmask=/sys/fs/cgroup --cpus 17.0 -it quay.io/podman/stable
podman --runtime runsc --runtime-flag cpu-num-from-quota run --cpus 4.0 --rm -it --network pasta:pasta:-o,172.16.0.2,-a,172.16.0.2,-n,24 --dns 8.8.8.8 --dns-search . docker.io/library/debian:sid-slim

init container with wg policy routing:

busybox ip link add wg0 type wireguard
busybox ip addr add 172.16.0.2/32 dev wg0
wg setconf wg0 /wg.conf
busybox ip link set wg0 up
busybox ip rule add from 172.16.0.2 lookup 80
busybox ip route add default dev wg0 table 80

---

We can use busybox init with custom /etc/inittab to have signal propagation (and proper stopping containers)

---

Mixing `--network` and `--pod` options of podman-create(1) makes container not joiningbpod namespace. It makes possible to mix no-network containers, multiple userspace network containers etc inside single pod.

---

Worker container initialization using scratch volume `/scratch` and additional podman image storage `/images`:

chown -R worker:worker /sys/fs/cgroup/
rm -rf /scratch/*
TMPDIR=/tmp exec su -s /bin/sh worker
podman run -it --network none 3257b7537dac /bin/sh

This way ephemeral containers and volumes get destroyed on each container restart.

---

To fix missing layers in nested imported images (stage2), they should be built with `--squash-all`.

---

Logs can be forwarded into files mounted as volume subpath: `-v vol:/stderr.txt:rw,subpath=/logs/stderr.txt`
The file must exist on volume though, so the trick is to create stdout/stderr/success/failure files for
every step in advance (in CREATE_THROWAWAY_VOLUME) and then delete zero-length files in ASSEMBLE_PROPOSAL_REPO
step.
